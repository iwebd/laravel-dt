<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 10/4/16
 * Time: 13:11
 */

namespace Lviv\DataTables\Decorators;

use Lviv\DataTables\Objects\Decorator;

class Image extends Decorator
{
    /**
     * @var
     */
    protected $type = Decorator::TYPE_IMAGE;
}