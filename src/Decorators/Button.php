<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 2/22/16
 * Time: 10:28
 */

namespace Lviv\DataTables\Decorators;

use Lviv\DataTables\Objects\Decorator;

/**
 * Class Button
 * @package Modules\DataTable\Decorators
 */
class Button extends Decorator
{
    /**
     * @var string
     */
    protected $type = Decorator::TYPE_BUTTON;
}