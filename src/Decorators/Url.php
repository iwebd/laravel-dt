<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 2/22/16
 * Time: 10:24
 */

namespace Lviv\DataTables\Decorators;

use Lviv\DataTables\Objects\Decorator;

/**
 * Class Url
 * @package Modules\DataTable\Decorators
 */
class Url extends Decorator
{
    /**
     * @var
     */
    protected $type = Decorator::TYPE_URL;

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var string
     */
    protected $text = '';

    /**
     * @var string
     */
    protected $field = '';

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        if(!empty($value['text'])){
            $this->setText($value['text']);
        }
        if(!empty($value['url'])){
            $this->setUrl($value['url']);
        }
        if(!empty($value['field'])){
            $this->setField($value['field']);
        }
        $this->value = $value;
    }

    /**
     * @param $url
     */
    public function setUrl($url){
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl(){
        return $this->url;
    }

    /**
     * @param $text
     */
    public function setText($text){
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText(){
        return $this->text;
    }

    /**
     * @param $field
     * @return $this
     */
    public function setField($field){
        $this->field = $field;
        return $this;
    }

    /**
     * @return string
     */
    public function getField(){
        return $this->field;
    }
}