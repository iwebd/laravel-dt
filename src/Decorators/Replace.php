<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 2/22/16
 * Time: 10:27
 */

namespace Lviv\DataTables\Decorators;

use Lviv\DataTables\Objects\Decorator;

/**
 * Class Replace
 * @package Modules\DataTable\Decorators
 */
class Replace extends Decorator
{
    /**
     * @var
     */
    protected $type = Decorator::TYPE_REPLACE;
}