<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/4/16
 * Time: 13:48
 */

namespace Lviv\DataTables\Objects;

class Filter
{
    protected $fieldKey;
    protected $type = '';
    protected $defaultValue;
    protected $placeholder;

    protected $attributes = [];

    const TYPE_INPUT = 'input';
    const TYPE_SELECT = 'select';
    const TYPE_DATE = 'date';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_HIDDEN = 'hidden';

    /**
     * @return mixed
     */
    public function getFieldKey()
    {
        return $this->fieldKey;
    }

    /**
     * @param mixed $fieldKey
     * @return $this
     */
    public function setFieldKey($fieldKey)
    {
        $this->fieldKey = $fieldKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param mixed $defaultValue
     * @return $this
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlaceholder()
    {
        return $this->placeholder;
    }

    /**
     * @param mixed $placeholder
     * @return $this
     */
    public function setPlaceholder($placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
        return $this;
    }

    public function getAttribute($key)
    {
        return (!empty($this->attributes[$key]) ? $this->attributes[$key] : false);
    }
}