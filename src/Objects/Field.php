<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 2/22/16
 * Time: 15:13
 */

namespace Lviv\DataTables\Objects;

class Field
{
    protected $id = '';
    protected $name = '';
    protected $key = '';
    protected $canSort = true;
    protected $defaultSort = '';
    protected $filter = null;
    protected $decorator = null;

    public function __construct(){
        $this->setId(md5(microtime()));
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this;
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return $this;
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isCanSort()
    {
        return $this->canSort;
    }

    /**
     * @param boolean $canSort
     * @return $this;
     */
    public function setCanSort($canSort)
    {
        $this->canSort = $canSort;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultSort()
    {
        return $this->defaultSort;
    }

    /**
     * @param string $defaultSort
     * @return $this;
     */
    public function setDefaultSort($defaultSort)
    {
        $this->defaultSort = $defaultSort;
        return $this;
    }

    /**
     * @return null
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @param Filter $filter
     * @return $this;
     */
    public function setFilter(Filter $filter)
    {
        $this->filter = $filter;
        return $this;
    }

    /**
     * @return null
     */
    public function getDecorator()
    {
        return $this->decorator;
    }

    /**
     * @param Decorator $decorator
     * @return $this;
     */
    public function setDecorator(Decorator $decorator)
    {
        $this->decorator = $decorator;
        return $this;
    }
}