<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/4/16
 * Time: 13:50
 */

namespace Lviv\DataTables\Objects;

class Decorator
{
    /**
     *
     */
    const TYPE_REPLACE = 'replace';
    /**
     *
     */
    const TYPE_URL = 'url';
    /**
     *
     */
    const TYPE_BUTTON = 'button';

    /**
     *
     */
    const TYPE_IMAGE = 'image';

    /**
     * @var
     */
    protected $type;

    /**
     * @var string
     */
    protected $key = '';
    /**
     * @var string
     */
    protected $value = '';

    /**
     *
     */
    public function __constructor(){
        $this->setKey(md5(microtime()));
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }
}