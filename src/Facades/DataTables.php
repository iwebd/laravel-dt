<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/4/16
 * Time: 13:33
 */

namespace Lviv\DataTables\Facades;

use Illuminate\Support\Facades\Facade;

class DataTables extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravel-dt';
    }

}