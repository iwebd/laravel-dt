<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/4/16
 * Time: 16:56
 */

namespace Lviv\DataTables\Traits;

use Illuminate\Http\Request;
use Lviv\DataTables\Builders\Query as DataTableQuery;
use Yajra\DataTables\Utilities\Request as DataTablesRequest;

trait ControllerTrait
{
    protected $_dataTable = null;
    protected $repository = null;

    protected function setDataTable($queryModel = null)
    {
        $dataTable = new DataTableQuery();

        if (!$queryModel) {
            $queryModel = $this->repository->getEntity()->query();
        }

        $dataTable->setQueryModel($queryModel);
        $this->_dataTable = $dataTable;

        return $this;
    }

    /**
     * @return DataTableQuery
     */
    protected function getDataTable()
    {
        if (!$this->_dataTable) {
            $this->setDataTable();
        }

        return $this->_dataTable;
    }

    public function dataTable(DataTablesRequest $request)
    {
        $dataTable = $this->getDataTable();
        $dataTable->setRequest($request);

        return $dataTable->build()->buildDataTable();
    }
}