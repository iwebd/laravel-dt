<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/4/16
 * Time: 13:21
 */

namespace Lviv\DataTables\Builders;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\DataTables\Utilities\Request as DataTablesRequest;

class Query
{
    /**
     * @var null
     */
    protected static $current = null;
    /**
     * @var
     */
    protected $request;
    /**
     * @var null
     */
    protected $queryModel = null;
    /**
     * @var array
     */
    protected $relations = [];
    /**
     * @var array
     */
    protected $relationFields = [];
    /**
     * @var array
     */
    protected $scopeFields = [];
    /**
     * @var array
     */
    protected $requestOffFields = [];

    protected $wheres = null;

    /**
     * @return null
     */
    public function getQueryModel()
    {
        return $this->queryModel;
    }

    /**
     * @param null $queryModel
     * @return $this
     */
    public function setQueryModel($queryModel)
    {
        $this->queryModel = $queryModel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     * @return $this
     */
    public function setRequest(DataTablesRequest $request)
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param $relations
     * @return $this
     */
    public function setRelations(array $relations = [])
    {
        $this->relations = $relations;
        return $this;
    }

    /**
     * @return array
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * @param $fields
     * @return $this
     */
    public function setRequestOffField(array $fields = [])
    {
        $this->requestOffFields = $fields;
        return $this;
    }

    /**
     * @return array
     */
    public function getRequestOffField()
    {
        return $this->requestOffFields;
    }

    /**
     * @return array
     */
    public function getRelationFields()
    {
        return $this->relationFields;
    }

    /**
     * @param array $relationFields
     * @return $this
     */
    public function setRelationFields(array $relationFields = [])
    {
        $this->relationFields = $relationFields;
        return $this;
    }

    /**
     * @param array $scopeFields
     * @return $this
     */
    public function setScopeFields(array $scopeFields = [])
    {
        $this->scopeFields = $scopeFields;
        return $this;
    }

    /**
     * @return array
     */
    public function getScopeFields()
    {
        return $this->scopeFields;
    }

    public function setWheres($wheres = null)
    {
        $this->wheres = $wheres;
        return $this;
    }

    public function getWheres()
    {
        return $this->wheres;
    }

    public function build()
    {
        $relations = $this->getRelations();
        $wheres = $this->getWheres();

        if (!empty($relations)) {
            call_user_func([$this->queryModel, 'with'], $relations);
        }

        if (!empty($wheres)) {
            call_user_func([$this->queryModel, 'where'], $wheres);
        }

        $request = $this->getRequest();
        $offFields = $this->getRequestOffField();
        $relationFields = $this->getRelationFields();
        $scopeFields = $this->getScopeFields();

        if (!empty($offFields) || !empty($relationFields) || !empty($scopeFields)) {
            $columns = array_map(function ($value) use ($offFields, $relationFields, $scopeFields) {
                if (in_array($value['name'], $offFields) ||
                    isset($relationFields[$value['name']]) ||
                    isset($scopeFields[$value['name']])
                ) {
                    if (isset($relationFields[$value['name']]) && !empty($value['search']['value'])) {
                        $rF = $relationFields[$value['name']]['field'];
                        $rC = $relationFields[$value['name']]['condition'];
                        $this->relationConstraints(
                            $this->queryModel,
                            $rF,
                            $value['search']['value'],
                            $rC
                        );
                    }
                    if (isset($scopeFields[$value['name']]) && !empty($value['search']['value'])) {

                        $f = $scopeFields[$value['name']];
                        $this->scopeConstraints(
                            $this->queryModel,
                            $f['field'],
                            $f['className'],
                            $f['scopeName'],
                            $f['scopeField'],
                            $value['search']['value']
                        );
                    }
                    $value['search']['value'] = '';
                }
                return $value;
            }, $request->get('columns'));
            $request->merge(['columns' => $columns]);
        }

        return $this;
    }

    public function buildDataTable(){
        $dataTable = new Datatables($this->getRequest());
        return $dataTable->eloquent($this->queryModel)->make(true);
    }

    protected function relationConstraints($query, $field, $value, $condition = 'like')
    {
        $pos = strpos($field, '.');
        if ($pos) {
            $relation = substr($field, 0, $pos);
            $query->whereHas($relation, function ($query) use ($field, $pos, $value, $condition) {
                return $this->relationConstraints($query, substr($field, $pos + 1), $value, $condition);
            });
        } else {
            if ($condition == 'like' || $condition == 'ilike') {
                $query->where($field, $condition, '%' . $value . '%');
            } else {
                $query->where($field, $condition, $value);
            }
        }
        return $query;
    }

    protected function scopeConstraints($query, $field, $className, $scopeName, $dataField, $scopeArgument)
    {
        $queryModel = call_user_func([$className, 'query']);

        $data = $queryModel->{$scopeName}($scopeArgument)->lists($dataField, $dataField)->toArray();
        $query->whereIn($field, array_filter($data));
        return $query;
    }
}