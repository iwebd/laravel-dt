<?php
/**
 * Created by PhpStorm.
 * User: donnie
 * Date: 4/4/16
 * Time: 13:31
 */

namespace Lviv\DataTables;

use Lviv\DataTables\Objects\Field;
use Lviv\DataTables\Objects\Decorator;
use Lviv\DataTables\Objects\Filter;

use Lviv\DataTables\Decorators\Image as DecoratorImage;
use Lviv\DataTables\Decorators\Button as DecoratorButton;
use Lviv\DataTables\Decorators\Replace as DecoratorReplace;
use Lviv\DataTables\Decorators\Url as DecoratorUrl;
use Lviv\DataTables\Filters\Date as FilterDate;
use Lviv\DataTables\Filters\Input as FilterInput;
use Lviv\DataTables\Filters\Select as FilterSelect;
use Lviv\DataTables\Filters\Hidden as FilterHidden;


class DataTables
{
    const TOOLS_SIDE_LEFT = 'left';
    const TOOLS_SIDE_RIGHT = 'right';

    protected $tableId = 'data-table';
    protected $tableName = '';
    protected $withUpdateButton = true;
    protected $scrollable = false;
    protected $fields = [];
    protected $sortField = 'id';
    protected $sortType = 'ASC';
    protected $checkBox = true;
    protected $dataUrl = '';
    protected $toolsDataField = 'id';
    protected $tools = [];
    protected $actions = [];
    protected $toolsSide = 'right';

    protected static $currentField = '';

    /**
     * @return string
     */
    public function getTableId()
    {
        return $this->tableId;
    }


    /**
     * @param $tableId
     * @return $this
     */
    public function setTableId($tableId)
    {
        $this->tableId = $tableId;
        return $this;
    }


    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     * @return $this
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isWithUpdateButton()
    {
        return $this->withUpdateButton;
    }

    /**
     * @param boolean $withUpdateButton
     * @return $this
     */
    public function setWithUpdateButton($withUpdateButton)
    {
        $this->withUpdateButton = $withUpdateButton;
        return $this;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     * @return $this
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }

    public function addField($key, $name, $canSort = true, $defSort = '')
    {
        $field = new Field();
        $field->setKey($key);
        $field->setName($name);
        $field->setCanSort($canSort ? true : false);

        if (!empty($defSort)) {
            $field->setDefaultSort($defSort == 'ASC' ? 'ASC' : 'DESC');
        }

        $id = $field->getId();

        self::$currentField = $id;

        $this->fields[$id] = $field;
        return $this;
    }

    public function addFilter($type, $placeholderOrValues = '', $defValue = '', $attributes = [])
    {
        if (isset($this->fields[self::$currentField])) {
            $field = &$this->fields[self::$currentField];
            switch ($type) {
                case Filter::TYPE_SELECT:
                    $filter = new FilterSelect();
                    if (!empty($placeholderOrValues) && is_array($placeholderOrValues)) {
                        $filter->setValues($placeholderOrValues);
                    }
                    break;
                case Filter::TYPE_DATE:
                    $filter = new FilterDate();
                    $filter->setAttribute('placeholder', $placeholderOrValues);
                    break;
                case Filter::TYPE_HIDDEN:
                    $filter = new FilterHidden();
                    break;
                default:
                    $filter = new FilterInput();
                    $filter->setAttribute('placeholder', $placeholderOrValues);
                    break;
            }
            foreach ($attributes as $key => $value) {
                $filter->setAttribute($key, $value);
            }
            $filter->setDefaultValue($defValue);
            $field->setFilter($filter);
        }
        return $this;
    }

    public function setDecorator($type, $values = [])
    {
        if (isset($this->fields[self::$currentField])) {
            $field = &$this->fields[self::$currentField];
            switch ($type) {
                case Decorator::TYPE_IMAGE:
                    $decorator = new DecoratorImage();
                    break;
                case Decorator::TYPE_BUTTON:
                    $decorator = new DecoratorButton();
                    break;
                case Decorator::TYPE_REPLACE:
                    $decorator = new DecoratorReplace();
                    break;
                case Decorator::TYPE_URL:
                    $decorator = new DecoratorUrl();
                    break;
                default:
                    $decorator = new DecoratorReplace();
                    break;
            }
            $decorator->setValue($values);
            $field->setDecorator($decorator);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getSortField()
    {
        return $this->orderField;
    }

    /**
     * @param string $sortField
     * @return $this
     */
    public function setSortField($sortField)
    {
        $this->sortField = $sortField;
        return $this;
    }

    /**
     * @return string
     */
    public function getSortType()
    {
        return $this->sortType;
    }

    /**
     * @param string $sortType
     * @return $this
     */
    public function setSortType($sortType)
    {
        $this->sortType = $sortType;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isCheckBox()
    {
        return $this->checkBox;
    }

    /**
     * @param boolean $checkBox
     * @return $this
     */
    public function setCheckBox($checkBox)
    {
        $this->checkBox = $checkBox;
        return $this;
    }

    public function setToolsSide($side = 'right')
    {
        $this->toolsSide = $side;
        return $this;
    }

    public function getToolsSide()
    {
        return $this->toolsSide;
    }

    /**
     * @return array
     */
    public function getTools()
    {
        return $this->tools;
    }

    public function addTools($name, $url, $buttonClass, $onClick = '')
    {
        $this->tools[$name] = [
            "url" => $url,
            'class' => $buttonClass,
            'name' => $name,
            'onclick' => $onClick,
        ];
        return $this;
    }

    /**
     * @param array $tools
     * @return $this
     */
    public function setTools($tools)
    {
        $this->tools = $tools;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataUrl()
    {
        return $this->dataUrl;
    }

    /**
     * @param string $dataUrl
     * @return $this
     */
    public function setDataUrl($dataUrl)
    {
        $this->dataUrl = $dataUrl;
        return $this;
    }

    public function addAction($name, $url)
    {
        $this->actions[$name] = $url;
        return $this;
    }

    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @return string
     */
    public function getToolsDataField()
    {
        return $this->toolsDataField;
    }

    /**
     * @param string $toolsDataField
     * @return $this
     */
    public function setToolsDataField($toolsDataField)
    {
        $this->toolsDataField = $toolsDataField;
        return $this;
    }
}