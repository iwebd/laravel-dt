<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 13.10.15
 * Time: 10:36
 */

namespace Lviv\DataTables\Filters;

use Lviv\DataTables\Objects\Filter;

/**
 * Class Select
 * @package Core\Helpers\DataTable\Filters
 */
class Select extends Filter
{
    /**
     * @var string
     */
    protected $type = Filter::TYPE_SELECT;
    /**
     * @var array
     */
    protected $values = [];

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function setValues($values = [])
    {
        $this->values = $values;
        return $this;
    }
}