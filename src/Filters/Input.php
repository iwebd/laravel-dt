<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 13.10.15
 * Time: 10:37
 */

namespace Lviv\DataTables\Filters;

use Lviv\DataTables\Objects\Filter;

/**
 * Class Input
 * @package Core\Helpers\DataTable\Filters
 */
class Input extends Filter
{
    /**
     * @var string
     */
    protected $type = Filter::TYPE_INPUT;

}