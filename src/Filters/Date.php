<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 13.10.15
 * Time: 10:37
 */

namespace Lviv\DataTables\Filters;

use Lviv\DataTables\Objects\Filter;

/**
 * Class Date
 * @package Core\Helpers\DataTable\Filters
 */
class Date extends Filter
{
    /**
     * @var string
     */
    protected $type = Filter::TYPE_DATE;
    /**
     * @var string
     */
    protected $format = 'YYYY.mm.dd';

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     * @return $this
     */
    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }


}