<?php
/**
 * Created by PhpStorm.
 * User: wiz-trans
 * Date: 09.01.16
 * Time: 16:46
 */

namespace Lviv\DataTables\Filters;

use Lviv\DataTables\Objects\Filter;

/**
 * Class Hidden
 * @package Core\Helpers\DataTable\Filters
 */
class Hidden extends Filter
{
    /**
     * @var string
     */
    protected $type = Filter::TYPE_HIDDEN;
}