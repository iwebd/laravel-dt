<script>
    var confirmDelete = function (obj) {
        if (!confirm('Вы уверены что хотите удалить?')) {
            return false;
        }
    }
</script>

<script>
    $(function () {
        var renderF = {};
        $.fn.dataTableExt.sErrMode = 'mute';
        _DataTable.setUrl('{!! $table->getDataUrl() !!}', '{{$table->getTableId()}}');
        _DataTable.setContainer($('#{{ $table->getTableId() }}'), '{{$table->getTableId()}}');
        @if($table->getFields())
        <?php $i = 0?>
                @foreach($table->getFields() as $field)
                @if($field->getDecorator())
                @if($field->getDecorator()->getType()=='replace')
                renderF['{{$field->getId()}}'] = function (data, type, full) {
            var values = {};
            @foreach($field->getDecorator()->getValue() as $value=>$replace)
                    values['{{$value}}'] = '{{$replace}}';
            @endforeach
                    return (values[data] ? values[data] : '' );
        };
        @elseif($field->getDecorator()->getType()=='button')
                renderF['{{$field->getId()}}'] = function (data, type, full) {
            var values = {};
            @foreach($field->getDecorator()->getValue() as $value=>$button)
                    values['{{$value}}'] = '<div class="{{!empty($button['class']) ? $button['class'] : 'btn btn-primary' }}">' +
                    '{{$button['text']}}' +
                    '</div>';
            @endforeach
                    return (values[data] ? values[data] : '' );
        };
        @elseif($field->getDecorator()->getType()=='image')
                renderF['{{$field->getId()}}'] = function (data, type, full) {
            var values = {};
            @foreach($field->getDecorator()->getValue() as $params)
                    values[data] = '<img class="{!! !empty($params['class']) ? $params['class'] . 'image-field' : 'image-field' !!}" alt="{!! !empty($params['alt']) ? $params['alt'] : '' !!}" src="'+data+'" />';
            @endforeach
                    return (values[data] ? values[data] : '' );
        };
        @elseif($field->getDecorator()->getType()=='url')
                renderF['{{$field->getId()}}'] = function (data, type, full) {
            if (!data) {
                return;
            }

            var url = '';
            var text = data;
            @if(!empty($field->getDecorator()->getText()))
                    text = '{{$field->getDecorator()->getText()}}';
            @endif
                    @if(!empty($field->getDecorator()->getUrl()))
                    url = '{{$field->getDecorator()->getUrl()}}';
            @if($field->getDecorator()->getField())
                    url += full['{{$field->getDecorator()->getField()}}'] ? full['{{$field->getDecorator()->getField()}}'] : data
            @else
                    url += data;
            @endif
            @endif
                    return '<a href="' + url + '" class="{{!empty($button['class']) ? $button['class'] : '' }}">' + text + '</a>';
        };
        @endif
    @endif
        _DataTable.addColumn(
                '{!! $field->getKey() !!}',
                '{!! $field->getKey() !!}',
                '{!! $field->isCanSort() ? 'true' : 'false' !!}',
                (renderF['{!!$field->getId()!!}'] ? renderF['{!! $field->getId() !!}'] : ''),
                '{{$table->getTableId()}}'
        );
        _DataTable.addFilter($('#filter_' + '{{ $field->getId() }}'), '{{$table->getTableId()}}');
        _DataTable.addOrder('{{$i}}', '{{ $field->getDefaultSort() }}', '{{$table->getTableId()}}');
        <?php $i++?>
        @endforeach
        @endif
        _DataTable.setToolsSide('{{$table->getTableId()}}', '{{$table->getToolsSide()}}');
        @foreach($table->getTools() as $tool)
        _DataTable.addTool('{{$tool['name']}}',
                '{{$tool['url']}}',
                '{{$tool['class']}}', '{{$tool['onclick']}}',
                '{{$table->getTableId()}}',
                '{{$table->getToolsDataField()}}'
        );
        @endforeach

        @if(!empty($table->getActions()))
        _DataTable.addButton('select_all', 'Выделить все', 'btn btn-primary', '', '{{$table->getTableId()}}');
        _DataTable.addButton('select_none', 'Снять выделение', 'btn btn-primary', '', '{{$table->getTableId()}}');
        @foreach($table->getActions() as $name=>$url)
        _DataTable.addButton('text', '{{$name}}', 'btn btn-primary', function (nButton, oConfig, oFlash) {
            if (window['confirmCustom'] && typeof window['confirmCustom'] == 'function') {
                if (confirmCustom()) {
                    _DataTable.request('{{$url}}', '{{$table->getTableId()}}');
                }
            } else {
                _DataTable.request('{{$url}}', '{{$table->getTableId()}}');
            }

        }, '{{$table->getTableId()}}');
        @endforeach
    @endif

    _DataTable.init('{{$table->getTableId()}}');

        @if($table->isWithUpdateButton())
        $('#' + '{{$table->getTableId()}}-update').on('click', function () {
            _DataTable.update('{{$table->getTableId()}}');
        });
        @endif

        $(".dataTables_length select").select2({
            minimumResultsForSearch: "-1"
        });

        $('.dataTables_filter input[type=search]').attr('placeholder', 'Введите значение для поиска...').addClass('form-control');

        $('input.datepicker').datepicker({
            autoclose: true,
            language: 'ru',
            format: 'yyyy-mm-dd'
        }).on("changeDate", function (e) {
            $(this).attr('value', e.currentTarget.value).keyup();
        });
    });
</script>
